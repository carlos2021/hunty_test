FROM python:3.10

RUN python -m pip install --upgrade pip

WORKDIR /app



COPY requirements-dev.txt /app/requirements-dev.txt
RUN python -m pip install -r requirements-dev.txt
COPY .env /app
COPY .sh /app
COPY ./app /app/app
COPY app/db/migrations /app/db/migrations
COPY alembic.ini /app
COPY prestart.sh /app

EXPOSE 8100

ENTRYPOINT ["uvicorn"]
CMD ["app.main:app", "--host", "0.0.0.0", "--port", "8100"]
