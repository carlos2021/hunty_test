# Prueba tecnica para hunty 

La idea es pasar a trabajar con hunty con ayuda de este proyecto


## Detalle

Crear un servicio en Python FastAPI que nos permita realizar un CRUD de los tipos de  información que maneja la API, vacantes, usuarios y empresas. Para ello se deben construir los endpoints necesarios.
 
La clase de Vacante debe tener como mínimo los siguientes atributos:
{
 "PositionName": "Python Dev",
 "CompanyName": "Test company",
 "Salary": 9999999,
 "Currency": COP,
 "VacancyId": uuidv4(),
 "VacancyLink": "https://www.test.com",
 "RequiredSkills": [
   {
     "Python": 1
   },
   {
     "NoSQL": 2
   }
 ]
}
 
la clase de Usuario debe tener como mínimo los siguientes atributos:
{
 "UserId": uuidv4(),
 "FirstName": "Test Name",
 "LastName": "Test Last Name",
 "Email": "un.test.no.hace.mal@gmail.com",
 "years_previous_experience": 5,
 "Skills": [
   {
     "Python": 1
   },
   {
     "NoSQL": 2
   }
 ]
}
 
 
Toda la API debe estar en un repositorio público de github, con readme, colección de postman y test unitarios. Debe ser minimalista, REST y lo más limpia, no hay limitantes de librerías a incorporar más allá de las restricciones que todos deben conocer al agregar librerías a proyectos.
 
Dentro de la descripción del problema, nos tomamos ciertas concesiones con respecto a formatos que dejaremos que utilices las mejores prácticas que conoces.
 
 
Además de los endpoints normales CRUD, dado un user, queremos conocer qué vacantes podrían ajustarse a su perfil de forma semántica sólo por el nombre de los skills y el rango mínimo de experiencia que pide la vacante por skill, comparado con lo que tiene el usuario, esto tiene solo las restricciones nombre de skill y experiencia mínima de skill, pero se le pueden recomendar a aquellos que no cumplan todas las restricciones pero si 50% o más de los requisitos (1 de 2, 2 de 3, 2 de 4, 3 de 5, etc).
Por ejemplo, un usuario conoce Python con 5 años de experiencia, conoce NoSQL con 4 años de experiencia y conoce AWS con 3 años de experiencia.
Mientras que una vacante pide como requisito Python con 3 años, NoSQL con 5 años y AWS con 2 años, esta vacante también se puede recomendar.

# Variables de entorno

Debes definir las siguientes variables:

- DB_USER: Corresponde al usuario con el que se manejará la base de datos que se esta creando de forma local.
- DB_PASSWORD: Corresponde a la constraseña que se dará a la base de datos que se esta creando de forma local.
- DB_HOST: Corresponde al host en la que podemos ubicar la base de datos.
- DB_NAME_HUNTY:  Corresponde a un nombre especifico que se dará la base de datos creada de forma local.



# con docker

## Docker

El proyecto esta fuentemente cimentado en el uso de Docker, y docker-compose, tan para los entornos de desarrollo, como producción. 


## Ponerlo a correr
    $ sudo docker-compose -f local.yml build
    $ sudo docker-compose -f local.yml up




## Despliegue a producción
    - monta la bd y le pone las credenciales en el .env de la bd
    $ sudo docker-compose -f production.yml build
    $ sudo docker-compose -f production.yml up
    



# con entorno
1. Crear entorno virtual:

```bash
python3  -m venv venv
```

2. Activar el entorno virual:

```bash
python -m venv/Script/activate
```

3. Instalar las dependencias necesarias:

```bash
python -m pip install -r requirements.txt -r requirements-dev.txt
```

4. Desplegar la base de datos de manera local:

```bash
sudo docker run --name postgres-db -e POSTGRES_PASSWORD=docker -e POSTGRES_DB=demo -e POSTGRES_USER=test -p 5432:5432 postgres
```

- si se le sale, asi la vuelve a levantar
```bash
sudo docker ps -a

sudo docker start <containerID>
```

5. Realizar la migración de la base de datos a su última versión:

```bash
python -m alembic upgrade head
```

6. Correr la aplicación y levantar servidor para los andpoints:

```bash
python -m uvicorn app.main:app --host 0.0.0.0 --port 8000
```

# Utilidades

```bash

python -m alembic init db-migration

python -m alembic revision --autogenerate -m "firstmg"

python -m alembic revision -m "timescaledb"

python -m alembic upgrade head

python -m alembic history - verbose

alembic revision --autogenerate -m "init"

alembic upgrade head

alembic init alembic

```
