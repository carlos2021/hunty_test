class Error(Exception):
    """Base class for other exceptions"""

    pass


class DuplicatedUuid(Error):
    """Raised when the input value is too small"""

    def __init__(self, message: str):
        self.message = message
        super().__init__(self.message)
