from typing import Optional, Union, Any

from fastapi import APIRouter, Depends, HTTPException, Response, status
from pydantic import UUID4
from sqlalchemy.orm import Session

from app.api.dependencies import get_db
from app.db.crud import users as crud
from app.models.schemas import users as schemas

router = APIRouter()


@router.post(
    "/users/",
    response_model=schemas.User,
    status_code=status.HTTP_201_CREATED,
)
def create_user(
    body: schemas.User,
    db: Session = Depends(get_db),
) -> schemas.User:
    user = crud.create_user(db=db, user=body)
    return user


@router.get(
    "/users/{id}/",
    response_model=schemas.User,
)
def read_user(
    id: UUID4,
    db: Session = Depends(get_db),
) -> Union[schemas.User, Response]:
    user = crud.read_user(db=db, id=id)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return user


@router.get("/users/")
def search_user(
    db: Session = Depends(get_db),
) -> Any:
    user = crud.search_user(db=db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return user


@router.delete("/users/{id}/")
def delete_user(
    id: UUID4,
    db: Session = Depends(get_db),
) -> Response:
    change = crud.delete_user(db=db, id=id)
    if not change:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.patch(
    "/users/{id}/",
    response_model=schemas.User,
)
def update_user(
    id: UUID4,
    change: schemas.UserUpdate,
    db: Session = Depends(get_db),
) -> Union[schemas.User, Response]:
    resource = crud.update_user(db=db, id=id)
    if not resource:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return crud.update_user(db=db, resource=resource, change=change)
