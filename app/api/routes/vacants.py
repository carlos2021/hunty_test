from typing import Optional, Union, Any

from fastapi import APIRouter, Depends, HTTPException, Response, status
from pydantic import UUID4
from sqlalchemy.orm import Session

from app.api.dependencies import get_db
from app.db.crud import vacants as crud
from app.db.crud import users as crud_user
from app.models.schemas import vacants as schemas
from app.utils.fit_profile import search_vacants_fit_profile

router = APIRouter()


@router.post(
    "/vacants/",
    response_model=schemas.Vacants,
    status_code=status.HTTP_201_CREATED,
)
def create_vacant(
    body: schemas.Vacants,
    db: Session = Depends(get_db),
) -> schemas.Vacants:
    vacant = crud.create_vacant(db=db, vacant=body)
    return vacant


@router.get(
    "/vacants/",
    response_model=schemas.Vacants,
)
def search_vacant(
    db: Session = Depends(get_db),
) -> Any:
    vacant = crud.search_vacant(db=db)
    if not vacant:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return vacant


@router.get(
    "/vacants/{id}/",
    response_model=schemas.Vacants,
)
def read_vacant(
    id: UUID4,
    db: Session = Depends(get_db),
) -> Union[schemas.Vacants, Response]:
    vacant = crud.read_vacant(db=db, id=id)
    if not vacant:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return vacant


@router.delete("/vacants/{id}/")
def delete_vacant(
    id: UUID4,
    db: Session = Depends(get_db),
) -> Response:
    vacant = crud.delete_vacant(db=db, id=id)
    if not vacant:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.patch(
    "/vacants/{id}/",
    response_model=schemas.VacantUpdate,
)
def update_frontier(
    id: UUID4,
    frontier: schemas.VacantUpdate,
    db: Session = Depends(get_db),
) -> Union[schemas.VacantUpdate, Response]:
    resource = crud.read_vacant(db=db, id=id)
    if not resource:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return crud.update_vacant(db=db, resource=resource, frontier=frontier)


@router.get("/fit-your-profile/{user_id}/")
def fit_your_profile(
    user_id: UUID4,
    db: Session = Depends(get_db),
) -> Union[schemas.VacantUpdate, Response]:
    info_user = crud_user.read_user(db=db, id=user_id)
    if not info_user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return search_vacants_fit_profile(db=db, user=info_user)
