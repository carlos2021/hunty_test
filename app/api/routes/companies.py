from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    status,
    Response,
)
from pydantic import UUID4
from sqlalchemy.orm import Session
from app.api.dependencies import get_db
from app.db.crud import companies as crud
from app.models.schemas import companies as schemas
from typing import Any, Optional

router = APIRouter()


@router.post(
    "/companies/",
    response_model=schemas.Company,
    status_code=status.HTTP_201_CREATED,
)
def create_company(
    data_company: schemas.Company,
    db: Session = Depends(get_db),
) -> Any:
    company = crud.create_company(db=db, data_company=data_company)
    return company


@router.patch(
    "/companies/{id}/",
    response_model=schemas.Company,
)
def update_company(
    id: UUID4,
    company: schemas.UpdateCompany,
    db: Session = Depends(get_db),
) -> Any:
    resource = crud.read_company(db=db, company_id=id)
    if not resource:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return crud.update_company(db=db, resource=resource, company=company)


@router.delete("/companies/{id}/")
def delete_company(
    id: UUID4,
    db: Session = Depends(get_db),
) -> Response:
    company = crud.delete_company(db=db, id=id)
    if not company:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.get("/companies/{id}/")
def get_company(
    id: UUID4,
    db: Session = Depends(get_db),
) -> schemas.Company:
    resource = crud.read_company(db=db, company_id=id)
    if not resource:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Resource Not Found"
        )
    return resource


@router.get("/companies/")
def search_company(
    db: Session = Depends(get_db),
) -> Any:
    resource = crud.search_company(db=db)
    return resource
