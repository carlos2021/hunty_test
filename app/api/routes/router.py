import logging

from fastapi import APIRouter

from app.api.routes import (
    companies,
    users,
    vacants,
)

logger = logging.getLogger(__name__)

api_router = APIRouter()
api_router.include_router(companies.router, tags=["Companies"])
api_router.include_router(users.router, tags=["Changes"])
api_router.include_router(vacants.router, tags=["Vacants"])
