import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from app.core.config import settings

POD_NAME: str = os.getenv("POD_NAME", "pod-demo")

engine = create_engine(
    settings.SQLALCHEMY_DATABASE_URL,
    pool_size=5,
    max_overflow=15,
    pool_recycle=120,  # seconds
    connect_args={
        "application_name": POD_NAME,
        # "keepalives": 1,
        # "keepalives_idle": 30,
        # "keepalives_interval": 10,
        # "keepalives_count": 5,
    },
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
