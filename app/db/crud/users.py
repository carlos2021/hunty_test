import logging
from typing import Any

from pydantic import UUID4
from sqlalchemy.orm import Query, Session

import app.models.huntys as models
import app.models.schemas.users as schemas
from app.db.crud import utils

logger = logging.getLogger(__name__)


def create_user(db: Session, user: schemas.User) -> models.User:
    logger.info("*** Creating ***")
    logger.debug(f"Incoming: {user.dict()}")
    user = models.User(**user.dict())
    return utils.add_resource(db, user)


def read_user(db: Session, id: UUID4) -> Any:
    logger.info(f"Reading the Change Type: {id}")
    return db.query(models.User).filter(models.User.user_id == id).first()


def delete_user(db: Session, id: UUID4) -> Any:
    user = db.query(models.User).filter(models.User.user_id == id).first()
    if user is None:
        return user
    else:
        return utils.delete_resource(user, db=db)


def update_user(
    db: Session,
    resource: models.User,
    user: schemas.UserUpdate,
) -> models.User:
    return utils.update_resource(
        db=db,
        resource=resource,
        update_data=user.dict(exclude_unset=True),
    )


def search_user(
    db: Session,
) -> Query:
    query = db.query(models.User)
    logger.info("*** Searching Companies***")
    query = query.filter().all()
    return query
