import logging
from typing import Any, Optional

from pydantic import UUID4
from sqlalchemy.orm import Query, Session

import app.models.huntys as models
import app.models.schemas.vacants as schemas
from app.db.crud import utils

logger = logging.getLogger(__name__)


def create_vacant(db: Session, vacant: schemas.Vacants) -> schemas.Vacants:
    logger.info("*** Creating ***")
    logger.debug(f"Incoming: {vacant.dict()}")
    vacant = models.Vacant(**vacant.dict())
    return utils.add_resource(db, vacant)


def read_vacant(db: Session, id: UUID4) -> models.Vacant:
    logger.info(f"Reading the Vacant Type: {id}")
    return db.query(models.Vacant).filter(models.Vacant.vacancy_id == id).first()


def search_vacant(
    db: Session,
) -> Query:
    query = db.query(models.Vacant)
    logger.info("*** Searching Companies***")
    query = query.filter().all()
    return query


def delete_vacant(db: Session, id: UUID4) -> models.Vacant:
    vacant = db.query(models.Vacant).filter(models.Vacant.vacancy_id == id).first()
    if vacant is None:
        return vacant
    else:
        return utils.delete_resource(vacant, db=db)


def update_vacant(
    db: Session,
    resource: models.Vacant,
    vacant: schemas.Vacants,
) -> models.Vacant:
    return utils.update_resource(
        db=db,
        resource=resource,
        update_data=vacant.dict(exclude_unset=True),
    )
