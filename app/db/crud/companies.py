import logging
from app.models.huntys import Company
import app.models.schemas.companies as schemas
from app.db.crud import utils
from typing import Any
from pydantic import UUID4
import app.models.huntys as models
from typing import Optional
from sqlalchemy.orm import Query, Session


logger = logging.getLogger(__name__)


def create_company(db: Session, data_company: schemas.Company) -> Any:
    logger.info("*** Creating ***")
    logger.debug(f"Incoming: {data_company.dict()}")
    data_company = models.Company(**data_company.dict())
    return utils.add_resource(db, data_company)


def read_company(db: Session, company_id: UUID4) -> models.Company:
    logger.info(f"Reading the certification: {company_id}")
    return (
        db.query(models.Company).filter(models.Company.company_id == company_id).first()
    )


def update_company(
    db: Session,
    resource: models.Company,
    company: schemas.Company,
) -> models.Company:
    return utils.update_resource(db=db, resource=resource, update_data=company.__dict__)


def search_company(
    db: Session,
) -> Query:
    query = db.query(models.Company)
    logger.info("*** Searching Companies***")
    query = query.filter().all()
    return query


def delete_company(db: Session, id: UUID4) -> models.Company:
    company = db.query(models.Company).filter(models.Company.company_id == id).first()
    if company is None:
        return company
    else:
        return utils.delete_resource(company, db=db)
