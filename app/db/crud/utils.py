import logging
from typing import Any, Dict

from fastapi import HTTPException, status
from sqlalchemy import exc
from sqlalchemy.orm import Session

from app.api.errors.errors import DuplicatedUuid

logger = logging.getLogger(__name__)


def create_resource(self: Any, db: Session) -> Any:
    logger.debug("**** Starting object create")
    db.add(self)
    try:
        db.commit()
        logger.debug("**** object was inserted")
        db.refresh(self)
        return self
    except exc.IntegrityError as error:
        db.rollback()
        db.close()
        logger.debug("########### Error unique key ##############")
        logger.debug(error.__dict__)
        raise DuplicatedUuid(message=str(error.orig))


def delete_resource(resource: Any, db: Session) -> Any:
    logger.debug("**** Starting object delete")
    db.delete(resource)
    db.commit()
    logger.debug("**** object was deleted")
    return resource


def add_resource(db: Session, resource: Any) -> Any:
    logger.info("*** Adding resource ***")
    try:
        logger.info("Resource was created")
        return resource.create_resource(db)
    except DuplicatedUuid as error:
        logger.debug(error.__dict__)
        raise HTTPException(status.HTTP_400_BAD_REQUEST, str(error.message))


def add_resources(db: Session, resources: Any) -> Any:
    logger.info("*** Adding resources ***")
    db.add_all(resources)
    try:
        db.commit()
        for resource in resources:
            db.refresh(resource)
        logger.info("Resources were created")
        return resources
    except DuplicatedUuid as error:
        logger.debug(error.__dict__)
        raise HTTPException(status.HTTP_400_BAD_REQUEST, str(error.message))


def update_resource(db: Session, resource: Any, update_data: Dict) -> Any:
    for key, value in update_data.items():
        setattr(resource, key, value)
    db.commit()
    db.refresh(resource)
    return resource
