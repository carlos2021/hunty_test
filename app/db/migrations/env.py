# from sqlalchemy import pool
import os
import sys
from logging.config import fileConfig

from alembic import context

# from sqlalchemy import engine_from_config
from sqlalchemy import create_engine

from app.core.config import settings
from app.db.conn import Base, SessionLocal, engine  # noqa: F401
from app.models.huntys import (  # noqa: F401
    User,
    Company,
    Vacant,
)

# from app.models import * # This helps alembic autogeneration


# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
base_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(base_dir)

target_metadata = Base.metadata


def get_url() -> str:
    # credentials = {
    # 'dbname': os.environ.get('DB_NAME_CAS'),
    # 'user': os.getenv('DB_USER'),
    # 'host': os.getenv('DB_HOST'),
    # 'password': os.getenv('DB_PASSWORD')
    # }
    # SQLALCHEMY_DATABASE_URL = f"postgresql+psycopg2://{credentials['user']}:{credentials['password']}@{credentials['host']}/{credentials['dbname']}"
    SQLALCHEMY_DATABASE_URL = settings.SQLALCHEMY_DATABASE_URL

    return SQLALCHEMY_DATABASE_URL


# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = get_url()
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
        compare_type=True,
        include_schemas=True,
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = create_engine(get_url())

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata,
            compare_type=True,
            include_schemas=True,
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()

else:
    run_migrations_online()
