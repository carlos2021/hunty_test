from typing import Dict

from fastapi.testclient import TestClient

from app.db.conn import Base, engine
from app.main import app

client = TestClient(app)

Base.metadata.create_all(bind=engine)


def test_create_demo() -> None:

    # Create settlement service point
    response = client.post(
        "/demos/",
        json={"attribute_1": "591f87e2-d087-11eb-b8bc-0242ac130003"},
    )
    data = response.json()
    assert response.status_code == 200
    assert data["attribute_1"] == "591f87e2-d087-11eb-b8bc-0242ac130003"


def test_read_demo(
    demo_info: Dict,
) -> None:

    demo_info_id = demo_info["id"]

    response = client.get(
        f"/demos/{demo_info_id}",
    )
    data = response.json()
    assert response.status_code == 200
    assert data["id"] == demo_info_id
    assert data["attribute_1"] == demo_info["attribute_1"]


def test_search_demos(
    demo_info: Dict,
) -> None:

    response = client.get(
        "/demos/",
        params={
            "attribute_1": demo_info["attribute_1"],
        },
    )

    data = response.json()
    found_demos = data["items"]
    assert response.status_code == 200
    assert demo_info["attribute_1"] in [demo["attribute_1"] for demo in found_demos]
