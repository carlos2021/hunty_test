from typing import Dict

import pytest
from fastapi.testclient import TestClient

from app.db.conn import Base, engine
from app.main import app

client = TestClient(app)
Base.metadata.create_all(bind=engine)


@pytest.fixture
def demo_info(scope: str = "session") -> Dict:
    response = client.post(
        "/demos/",
        json={
            "attribute_1": "demo",
        },
    )
    data = response.json()
    return data
