import os

from pydantic import BaseSettings

credentials = {
    "dbname": os.environ.get("DB_NAME_HUNTY"),
    "user": os.getenv("DB_USER"),
    "host": os.getenv("DB_HOST"),
    "password": os.getenv("DB_PASSWORD"),
}


class Settings(BaseSettings):
    SQLALCHEMY_DATABASE_URL: str = f"postgresql+psycopg2://{credentials['user']}:{credentials['password']}@{credentials['host']}/{credentials['dbname']}"
    CONTAINER_NAME: str = "frontier-documents"
    VERSION: str = "0.0.1"
    PROJECT_NAME: str = "Hunty - Manager"
    DESCRIPTION: str = "Restful API - Hunty Manager"


settings = Settings()
