import datetime as dt
from typing import Optional, Dict

from pydantic import UUID4, BaseModel


class VacantUpdate(BaseModel):
    vacancy_id: UUID4
    positionName: Optional[str]
    companyName: Optional[str]
    salary: Optional[float]
    currency: Optional[str]
    vacancyLink: Optional[str]
    requiredSkills: Optional[Dict] = None


class Vacants(BaseModel):
    vacancy_id: UUID4
    positionName: str
    companyName: str
    salary: float
    currency: str
    vacancyLink: str
    requiredSkills: Optional[Dict] = None

    class Config:
        orm_mode = True
