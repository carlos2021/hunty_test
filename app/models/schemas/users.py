import datetime as dt
from typing import Optional, Dict

from pydantic import UUID4, BaseModel


class User(BaseModel):
    user_id: UUID4
    first_name: str
    last_name: str
    email: str
    years_previous_experience: str
    skills: Optional[Dict] = None

    class Config:
        orm_mode = True


class UserUpdate(BaseModel):
    first_name: Optional[str]
    last_name: Optional[str]
    email: Optional[str]
    years_previous_experience: Optional[str]
    skills: Optional[Dict] = None
