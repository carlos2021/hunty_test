import datetime as dt
from typing import Optional

from pydantic import UUID4, BaseModel


class Company(BaseModel):
    company_id: UUID4
    name: str
    address: str

    class Config:
        orm_mode = True


class UpdateCompany(BaseModel):
    name: Optional[str]
    address: Optional[str]
