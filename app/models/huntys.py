import datetime as dt
import logging
import uuid
from sqlalchemy import Column, ForeignKey, String, Float
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.sqltypes import JSON
from sqlalchemy.types import DateTime

from app.db.conn import Base
from app.db.crud import utils

logger = logging.getLogger(__name__)

UUID_RANDOM_DB = "uuid_generate_v4()"


Base.create_resource = utils.create_resource
Base.delete_resource = utils.delete_resource


class User(Base):
    __tablename__ = "users"
    user_id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        nullable=False,
        comment="Codigo semantico identificador del usuario",
    )
    first_name = Column(String, nullable=False, comment="nombres del usuario")
    last_name = Column(String, nullable=False, comment="apellidos del usuario")
    email = Column(String, nullable=False, comment="email del usuario")
    years_previous_experience = Column(
        String, nullable=False, comment="años de experiencia del usuario"
    )
    skills = Column(
        JSON,
        nullable=False,
        comment="habilidades que maneja el usuario",
    )
    # changes: relationship = relationship("Change", back_populates="change_type")


class Vacant(Base):
    __tablename__ = "vacant"
    vacancy_id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        comment="ID de usuario que creo la vacante",
    )
    positionName = Column(String, nullable=False, comment="posicion de la vacante")
    companyName = Column(
        String, nullable=False, comment="mombre de la compañia que ofrece la vacante"
    )
    salary = Column(
        Float, nullable=False, comment="salario que se ofrece por la vacante"
    )
    currency = Column(String, nullable=False, comment="divisa que ofrece la vacante")

    requiredSkills = Column(
        JSON,
        nullable=False,
        comment="habilidades necesarios para la vacante",
    )
    vacancyLink = Column(String, nullable=False, comment="link de la vacante")


class Company(Base):
    __tablename__ = "company"

    company_id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        comment="ID de la compañia que creo el registro",
    )
    name = Column(String, nullable=False, comment="Nombre de la empresa")
    address = Column(String, nullable=False, comment="direccion de la empresa")
