from app.models.schemas.users import User
from app.models.schemas.vacants import Vacants
from app.db.crud.vacants import search_vacant

from fastapi import Depends
from pydantic import UUID4
from sqlalchemy.orm import Session
from app.api.dependencies import get_db
from hermetrics.hamming import Hamming

def compare_semantic_json(user_skill, vacant_skill):
    coincide = 0
    for key, expeiencie_user in user_skill.items():
        for key2, expeiencie_requeried in vacant_skill.items():
            ham = Hamming()
            if ham.similarity(key, key2) > 0.8: 
                if expeiencie_requeried <= expeiencie_user:
                    coincide += 1
    return True if len(vacant_skill) <= coincide*2 else False


def search_vacants_fit_profile(db: Session, user: User) -> list[Vacants]:
    all_vacants = search_vacant(db=db)
    list_vacants_fit = [
        vacant
        for vacant in all_vacants
        if compare_semantic_json(user.skills, vacant.requiredSkills)
    ]

    return list_vacants_fit
